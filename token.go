package bogo

import (
	"bytes"
	"crypto/rand"
	"encoding/base32"
	"errors"
	"math/big"
	"strings"
	"time"
)

var ErrInvalidToken = errors.New("invalid token")
var tokenStore TokenStore

type TokenInfo struct {
	Token   string
	UserId  string
	Created time.Time
}

type TokenStore interface {
	Set(c *Context, info *TokenInfo) error
	Get(c *Context, token string) (*TokenInfo, error)
	Delete(c *Context, token string) error
}

func SetTokenStore(store TokenStore) {
	tokenStore = store
}

func getTokenStore() TokenStore {
	if tokenStore == nil {
		tokenStore = createMemoryTokenStore()
	}
	return tokenStore
}

type memoryTokenStore struct {
	values map[string]*TokenInfo
}

func createMemoryTokenStore() TokenStore {
	return &memoryTokenStore{values: make(map[string]*TokenInfo)}
}

func (s *memoryTokenStore) Set(c *Context, info *TokenInfo) error {
	s.values[info.Token] = info
	return nil
}

func (s *memoryTokenStore) Get(c *Context, key string) (info *TokenInfo, err error) {
	info, ok := s.values[key]
	if !ok {
		return nil, ErrInvalidToken
	}
	return info, nil
}

func (s *memoryTokenStore) Delete(c *Context, token string) error {
	delete(s.values, token)
	return nil
}

func createToken(length int) string {
	start := big.NewInt(0).Exp(big.NewInt(32), big.NewInt(int64(length-2)), nil)
	n := big.NewInt(0).Mul(start, big.NewInt(32))
	n.Sub(n, start)
	token, _ := rand.Int(rand.Reader, n)
	token.Add(token, start)
	tokenBytes := token.Bytes()
	encoding := base32.NewEncoding("abcdefghklmnpqrstuvwxyz123456789")
	encodedLen := encoding.EncodedLen(len(tokenBytes))
	out := make([]byte, encodedLen)
	encoding.Encode(out, tokenBytes)
	tokenString := bytes.NewBuffer(out).String()
	return strings.TrimRight(tokenString, "=")
}
