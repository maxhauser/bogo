package bogo

import (
	"io"
	"net/http"
)

type textResult string

func (ctx *Context) Text(text string) ActionResult {
	return textResult(text)
}

func (res textResult) Execute(r *http.Request, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	io.WriteString(w, string(res))
}
