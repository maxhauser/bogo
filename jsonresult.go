package bogo

import (
	"encoding/json"
	"io"
	"net/http"
)

type jsonResult struct {
	body    interface{}
	context *Context
}

func (ctx *Context) Json(dto interface{}) ActionResult {
	return &jsonResult{context: ctx, body: dto}
}

func (result *jsonResult) Execute(r *http.Request, w http.ResponseWriter) {

	cb := r.URL.Query().Get("callback")
	jsonp := cb != ""

	var ct string
	if jsonp { // jsonp
		ct = "text/javascript; charset=utf-8"
	} else {
		ct = "application/json; charset=utf-8"
	}

	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Content-Type", ct)
	if jsonp {
		io.WriteString(w, cb+"(")
	}

	if serializer, ok := result.body.(JsonSerializer); ok {
		serializer.WriteJson(result.context, &JsonWriter{Writer: w})
	} else {
		encoder := json.NewEncoder(w)
		encoder.Encode(result.body)
	}

	if jsonp {
		io.WriteString(w, ");")
	}
}
