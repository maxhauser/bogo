package bogo

import (
	"net/http"
	"strings"
)

type RegistrationHandler func(registrationContext *RegistrationContext)

func Register(pattern string, registrationHandler RegistrationHandler) {
	pattern = prepareRegistrationPattern(pattern)
	ctx := newRegistrationContext(pattern)
	registrationHandler(ctx)
	http.Handle(pattern, ctx)
}

func prepareRegistrationPattern(pattern string) string {
	if !strings.HasPrefix(pattern, "/") {
		pattern = "/" + pattern
	}
	if !strings.HasSuffix(pattern, "/") {
		pattern = pattern + "/"
	}
	return pattern
}
