package gae

import (
	"appengine"
	"appengine/memcache"
	"bogo"
	"strings"
	"time"
	"encoding/gob"
)

func init() {
	gob.Register(time.Time{})
	bogo.SetTokenStore(new(tokenStore))
}

type tokenStore struct{}

func (s *tokenStore) Get(c *bogo.Context, token string) (info *bogo.TokenInfo, err error) {
	cacheKey := getTokenCacheKey(c, token)
	info = new(bogo.TokenInfo)
	_, err = memcache.Gob.Get(GetAppEngineCtx(c), cacheKey, info)
	if err == memcache.ErrCacheMiss {
		err = bogo.ErrInvalidToken
	}
	if err != nil {
		info = nil
	}
	return
}

func (s *tokenStore) Set(c *bogo.Context, info *bogo.TokenInfo) error {
	err := memcache.Gob.Set(GetAppEngineCtx(c), &memcache.Item{
		Key:    getTokenCacheKey(c, info.Token),
		Object: info,
	})
	return err
}

func (s *tokenStore) Delete(c *bogo.Context, token string) error {
	cacheKey := getTokenCacheKey(c, token)
	return memcache.Delete(GetAppEngineCtx(c), cacheKey)
}

var instanceId string

func GetInstanceId(c *bogo.Context) string {
	if instanceId == "" {
		version := appengine.VersionID(GetAppEngineCtx(c))
		sep := strings.LastIndex(version, ".")
		version = version[:sep]
		instanceId = version
	}
	return instanceId
}

func getTokenCacheKey(c *bogo.Context, token string) string {
	return GetInstanceId(c) + ":token:" + token
}
