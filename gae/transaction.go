package gae

import (
	"appengine"
	"appengine/datastore"
	"bogo"
)

const inTransactionKey = "gaeInTransaction"

func RunInTransaction(c *bogo.Context, fn func(c *bogo.Context) error) error {
	inTransaction, ok := c.Items[inTransactionKey]
	if ok && inTransaction.(bool) {
		return fn(c)
	}
	cTrans := c.Clone()
	cTrans.Items[inTransactionKey] = true
	err := datastore.RunInTransaction(GetAppEngineCtx(c), func(c appengine.Context) error {
		cTrans.Items[appengineContextKey] = c
		return fn(cTrans)
	}, nil)

	return err
}
