package gae

import (
	"appengine"
	"bogo"
)

const appengineContextKey = "gaeContext"

func init() {
	bogo.OnCreateContext(func(c *bogo.Context) {
		c.Items[appengineContextKey] = appengine.NewContext(c.Request)
	})
}

func GetAppEngineCtx(c *bogo.Context) appengine.Context {
	return c.Items[appengineContextKey].(appengine.Context)
}
