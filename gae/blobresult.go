package gae

import (
	"appengine"
	"appengine/blobstore"
	"bogo"
	"net/http"
)

type blobResult struct {
	key appengine.BlobKey
}

func Blob(key appengine.BlobKey) bogo.ActionResult {
	return &blobResult{key}
}

func (res *blobResult) Execute(r *http.Request, w http.ResponseWriter) {
	blobstore.Send(w, res.key)
}
