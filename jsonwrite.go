package bogo

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
)

type JsonSerializer interface {
	WriteJson(*Context, *JsonWriter)
}

type transitionError struct {
	pos int
	source, target state
}

func (err transitionError) Error() string {
	return fmt.Sprintf("Invalid state transition at position %d from %s to %s", err.pos, stateNames[err.source], stateNames[err.target])
}

type state int

const (
	start = state(iota)
	object
	objectEnd
	array
	arrayEnd
	delimiter
	property
	value
)

var stateNames = []string{
	start:     "start",
	object:    "object",
	objectEnd: "objectEnd",
	array:     "array",
	arrayEnd:  "arrayEnd",
	delimiter: "delimiter",
	property:  "property",
	value:     "value",
}

var validtransitions = [][]state{
	start: {},

	object:    {start, property, delimiter, array},
	objectEnd: {object, value, arrayEnd, objectEnd},

	property: {delimiter, object},
	value:    {property, array, delimiter},

	array:    {start, property, delimiter, array},
	arrayEnd: {array, value, arrayEnd, objectEnd},

	delimiter: {value, arrayEnd, objectEnd},
}

type JsonWriter struct {
	io.Writer
	state state
	pos   int
}

func (w *JsonWriter) transition(target state) {
	for _, state := range validtransitions[target] {
		if state == w.state {
			w.state = target
			return
		}
	}
	panic(transitionError{w.pos, w.state, target})
}

func (w *JsonWriter) StartObject() {
	w.autoDelimiter()
	w.transition(object)
	w.Write([]byte("{"))
	w.pos++
}

func (w *JsonWriter) EndObject() {
	w.transition(objectEnd)
	w.Write([]byte("}"))
	w.pos++
}

func (w *JsonWriter) StartArray() {
	w.autoDelimiter()
	w.transition(array)
	w.Write([]byte("["))
	w.pos++
}

func (w *JsonWriter) EndArray() {
	w.transition(arrayEnd)
	w.Write([]byte("]"))
	w.pos++
}

func JsonEscapeString(s string) []byte {
	d := []byte(s)
	for i := 0; i < len(d); i++ {
		c := d[i]
		if bytes.IndexByte([]byte("\"\\\b\f\n\r\t/"), c) != -1 {
			if cap(d) <= len(d) {
				d2 := make([]byte, len(d)+1, len(d)+32)
				copy(d2, d[:i])
				copy(d2[i+1:], d[i:])
				d = d2
			} else {
				d = d[:len(d)+1]
				copy(d[i+1:], d[i:])
			}
			d[i] = '\\'
			i++
		}
	}
	return d
}

func (w *JsonWriter) autoDelimiter() {
	if w.state == value || w.state == objectEnd || w.state == arrayEnd {
		w.Delimiter()
	}
}

func (w *JsonWriter) String(s string) {
	w.autoDelimiter()
	w.transition(value)
	w.Write([]byte("\""))
	json := JsonEscapeString(s)
	w.Write(json)
	w.pos += len(json) + 2
	w.Write([]byte("\""))
}

func (w *JsonWriter) Number(n int64) {
	w.autoDelimiter()
	w.transition(value)
	json := []byte(strconv.FormatInt(n, 10))
	w.Write(json)
	w.pos += len(json)
}

func (w *JsonWriter) Bool(b bool) {
	w.autoDelimiter()
	w.transition(value)
	if b {
		w.Write([]byte("true"))
		w.pos += 4
	} else {
		w.Write([]byte("false"))
		w.pos += 5
	}
}

func (w *JsonWriter) Delimiter() {
	w.transition(delimiter)
	w.Write([]byte(","))
	w.pos ++
}

func (w *JsonWriter) PropertyName(name string) {
	w.autoDelimiter()
	w.transition(property)
	w.Write([]byte("\""))
	json := []byte(JsonEscapeString(name))
	w.Write(json)
	w.pos += len(json) + 3
	w.Write([]byte("\":"))
}

func (w *JsonWriter) StringProperty(name string, value string) {
	w.PropertyName(name)
	w.String(value)
}

func (w *JsonWriter) NumberProperty(name string, value int64) {
	w.PropertyName(name)
	w.Number(value)
}

func (w *JsonWriter) BoolProperty(name string, value bool) {
	w.PropertyName(name)
	w.Bool(value)
}

func (w *JsonWriter) Raw(json string) {
	data := []byte(json)
	w.Write(data)
	w.pos += len(data)
}
