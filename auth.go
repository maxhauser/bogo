package bogo

import (
	"net/http"
	"time"
)

const authTokenName = "auth_token"

func SetAuthCookie(c *Context, userId string) error {
	token, err := CreateAuthToken(c, userId)
	if err != nil {
		return err
	}
	http.SetCookie(c.responseWriter, &http.Cookie{
		Name:     authTokenName,
		Value:    token,
		HttpOnly: true,
		Path:     "/"})
	return nil
}

func removeAuthCookie(c *Context, token string) {
	getTokenStore().Delete(c, token)
	http.SetCookie(c.responseWriter, &http.Cookie{
		Name:     authTokenName,
		HttpOnly: true,
		Path:     "/",
		MaxAge:   -1})
}

func getAuthToken(r *http.Request) string {
	token := r.URL.Query().Get(authTokenName)

	if token == "" {
		cookie, _ := r.Cookie(authTokenName)
		if cookie != nil {
			token = cookie.Value
		}
	}

	return token
}

func ValidateRequest(c *Context, r *http.Request) (userId string, err error) {
	token := getAuthToken(r)

	if token == "" {
		return "", ErrInvalidToken
	}

	return validateToken(c, token)
}

func RemoveAuthToken(c *Context) {
	token := getAuthToken(c.Request)
	if token == "" {
		return
	}
	removeAuthCookie(c, token)
}

func CreateAuthToken(c *Context, userId string) (string, error) {
	token := createToken(15)
	info := &TokenInfo{Token: token, UserId: userId, Created: time.Now()}
	if err := getTokenStore().Set(c, info); err != nil {
		return "", err
	}
	return token, nil
}

func validateToken(c *Context, token string) (userId string, err error) {
	var info *TokenInfo
	if info, err = getTokenStore().Get(c, token); err == nil {
		userId = info.UserId
	}
	return
}
