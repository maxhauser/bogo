package bogo

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strings"
)

type (
	RegistrationContext struct {
		pattern       string
		registrations []*registration
	}

	ActionResult interface {
		Execute(r *http.Request, w http.ResponseWriter)
	}

	Handler func(context *Context) ActionResult

	Context struct {
		Request        *http.Request
		UserId         string
		Captures       []string
		responseWriter http.ResponseWriter
		Items          map[string]interface{}
	}
)

func (c *Context) Clone() *Context {
	var newCtx = *c;
	items := make(map[string]interface{})
	for k,v := range c.Items {
		items[k] = v
	}
	newCtx.Items = items
	return &newCtx
}

func (context *Context) GetJsonBody(value interface{}) error {
	body := context.Request.Body
	defer body.Close()
	decoder := json.NewDecoder(body)
	err := decoder.Decode(value)
	return err
}

func (context *Context) SetHeader(name string, value string) {
	context.responseWriter.Header().Set(name, value)
}

func (context *Context) CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func (c *Context) Log(msg string, args ...interface{}) {
	log.Printf(msg+"\n", args...)
}

type registration struct {
	pattern        string
	regex          *regexp.Regexp
	method         string
	handler        Handler
	allowAnonymous bool
}

func newRegistrationContext(pattern string) *RegistrationContext {
	return &RegistrationContext{registrations: make([]*registration, 0, 10), pattern: pattern}
}

func preparePattern(pattern string) string {
	return "^" + strings.Replace(pattern, "?", "([^/]+)", -1) + "$"
}

func (ctx *RegistrationContext) Get(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "GET", handler: handler, allowAnonymous: false}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) GetAnonymous(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "GET", handler: handler, allowAnonymous: true}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) Post(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "POST", handler: handler, allowAnonymous: false}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) PostAnonymous(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "POST", handler: handler, allowAnonymous: true}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) Put(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "PUT", handler: handler, allowAnonymous: false}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) PutAnonymous(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "PUT", handler: handler, allowAnonymous: true}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) Delete(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "DELETE", handler: handler, allowAnonymous: false}
	ctx.registrations = append(ctx.registrations, registration)
}

func (ctx *RegistrationContext) DeleteAnonymous(pattern string, handler Handler) {
	pattern = preparePattern(pattern)
	registration := &registration{pattern: pattern, regex: regexp.MustCompile(pattern), method: "DELETE", handler: handler, allowAnonymous: true}
	ctx.registrations = append(ctx.registrations, registration)
}

func hasBody(method string) bool {
	return method == "POST" || method == "PUT"
}

type CreateContextCallback func(*Context)

var createContextCallbacks []CreateContextCallback

func OnCreateContext(cb CreateContextCallback) {
	createContextCallbacks = append(createContextCallbacks, cb)
}

func createContext(w http.ResponseWriter, r *http.Request, allowAnonymous bool) (*Context, error) {
	context := &Context{Request: r, responseWriter: w, Items: make(map[string]interface{})}
	for _, cb := range createContextCallbacks {
		cb(context)
	}

	userId, err := ValidateRequest(context, r)
	if !allowAnonymous {
		if err == ErrInvalidToken {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return nil, err
		}
		if err != nil {
			serverError(context, w, err)
			return nil, err
		}
	}

	if err == nil {
		context.UserId = userId
	} else {
		log.Printf("error: %s", err)
	}

	return context, nil
}

func serverError(c *Context, w http.ResponseWriter, err error) {
	msg := "Internal Server Error"
	if err != nil {
		msg += "(" + err.Error() + ")"
	}
	c.Log(msg)
	http.Error(w, msg, http.StatusInternalServerError)
}

func (ctx *RegistrationContext) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	methodNotAllowed := false
	for _, reg := range ctx.registrations {
		if match := reg.regex.FindStringSubmatch(r.URL.Path[len(ctx.pattern):]); match != nil {
			if reg.method != r.Method {
				methodNotAllowed = true
				continue
			}

			context, err := createContext(w, r, reg.allowAnonymous)
			if err != nil {
				return
			}
			context.Captures = match

			defer func() {
				if err, ok := recover().(error); ok {
					serverError(context, w, err)
				}
			}()

			result := reg.handler(context)
			if result != nil {
				result.Execute(r, w)
				return
			}
		}
	}
	if methodNotAllowed {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	} else {
		http.NotFound(w, r)
	}
}
