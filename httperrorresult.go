package bogo

import (
	"log"
	"net/http"
)

type httpErrorResult struct {
	statusCode int
	message    string
}

func (result *httpErrorResult) Execute(r *http.Request, w http.ResponseWriter) {
	http.Error(w, result.message, result.statusCode)
}

func getMessage(caption string, err error) string {
	if err == nil {
		return caption
	}
	return caption + " (" + err.Error() + ")"
}

func (context *Context) NotFound() ActionResult {
	return &httpErrorResult{statusCode: http.StatusNotFound, message: "Not Found"}
}

func (context *Context) Forbidden() ActionResult {
	return &httpErrorResult{statusCode: http.StatusForbidden, message: "Forbidden"}
}

func (context *Context) HttpError(statusCode int, message string) ActionResult {
	return &httpErrorResult{statusCode: statusCode, message: message}
}

func (context *Context) ServerError(err error) ActionResult {
	msg := getMessage("Internal Server Error", err)
	log.Println(msg)
	return &httpErrorResult{statusCode: http.StatusInternalServerError, message: msg}
}

func (context *Context) BadRequest(err error) ActionResult {
	msg := getMessage("Bad Request", err)
	log.Println(msg)
	return &httpErrorResult{statusCode: http.StatusBadRequest, message: msg}
}

func (context *Context) Ok() ActionResult {
	return &httpErrorResult{statusCode: http.StatusOK, message: "OK"}
}

func (context *Context) NoContent() ActionResult {
	return &httpErrorResult{statusCode: http.StatusNoContent, message: "No Content"}
}
