package bogo

import (
	"fmt"
	"runtime"
)

type Error string
var stackBuf = make([]byte, 1024)
var currStackBuf []byte 

func (e Error) Error() string {
	if currStackBuf == nil {
		return string(e)
	}
	return fmt.Sprintf("%s\n%s", string(e), string(currStackBuf))
}

func (e Error) Raise() error {
	l := runtime.Stack(stackBuf, false)
	currStackBuf = stackBuf[:l]
	return e 
}
